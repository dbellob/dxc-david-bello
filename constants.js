//aplication constants
const PATH = '/test';
const LISTEN_PORT = 3000;

//http status constants
const HTTP_STATUS_CODE_200 = 200;
const HTTP_STATUS_CODE_422 = 422;
const HTTP_STATUS_CODE_500 = 500;

//http status messages
const HTTP_STATUS_MESSAGE_500 = 'internal_server_error';
const HTTP_STATUS_MESSAGE_422 = 'invalid_data_format'

//number constants
const INT_VALUE_ZERO = 0;
const INT_VALUE_ONE = 1;

//string constants
const EMPTY_MESSAGE = '';
const UNDEFINED_DIVISION_MESSAGE = "indefinido";
const TYPE_OF_NUMBER = 'number';
const LISTEN_MESSAGE = 'La aplicación arrancó en el puerto: ';

module.exports = {
    PATH, LISTEN_PORT, HTTP_STATUS_CODE_200, HTTP_STATUS_CODE_422, HTTP_STATUS_CODE_500,
    HTTP_STATUS_MESSAGE_500, HTTP_STATUS_MESSAGE_422, INT_VALUE_ZERO, INT_VALUE_ONE, EMPTY_MESSAGE,
    UNDEFINED_DIVISION_MESSAGE, TYPE_OF_NUMBER, LISTEN_MESSAGE
}
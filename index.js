const express = require("express");
var bodyParser = require('body-parser');
const operations = require('./operations.js');
const constants = require('./constants');
const app = express();
app.use(bodyParser.json());

app.post(constants.PATH, function (req, res) {
    try {
        let inData = req.body.numeros;
        if (operations.validateInData(inData)) {
            statusCode = constants.HTTP_STATUS_CODE_200;
            operationsData = {
                suma: operations.suma(inData),
                resta: operations.resta(inData),
                multiplicacion: operations.multiplicacion(inData),
                division: operations.division(inData)
            };
            errorData = [];

        } else {
            statusCode = constants.HTTP_STATUS_CODE_422;;
            operationsData = constants.EMPTY_MESSAGE;
            errorData = [constants.HTTP_STATUS_MESSAGE_422];
        }
    }
    catch (error) {
        statusCode = constants.HTTP_STATUS_CODE_500;;
        operationsData = constants.EMPTY_MESSAGE;
        errorData = [constants.HTTP_STATUS_MESSAGE_500];

    }
    res.status(statusCode).json(
        {
            data: operationsData,
            errors: errorData
        }
    );
});

app.listen(constants.LISTEN_PORT, () => {
    console.log(constants.LISTEN_MESSAGE + constants.LISTEN_PORT);
});
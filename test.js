const operations = require('./operations.js')
const constants = require('./constants');
const assert = require('assert');

it('Validar suma entre números: 1,2,3', () => {
  assert.equal(operations.suma([1, 2, 3]), 6);
})

it('Validar resta entre los números: 1,2,3', () => {
  assert.equal(operations.resta([1, 2, 3]), -4);
})

it('Validar multiplicación entre los números: 1,2,3', () => {
  assert.equal(operations.multiplicacion([1, 2, 3]), 6);
})

it('Validar división entre los números: 1,2,3', () => {
  assert.equal(operations.division([1, 2, 3]), 0.16666666666666666);
})

it('Validar división cuando es sobre 0', () => {
  assert.equal(operations.division([1, 2, 0]), constants.UNDEFINED_DIVISION_MESSAGE);
})

it('Validar método validateInData() cuando los datos son correctos', () => {
  assert.equal(operations.validateInData([1, 2, 3]), true);
})

it('Validar método validateInData() cuando existe un dato no numérico en el Array', () => {
  assert.equal(operations.validateInData([1, "2", 3]), false);
})

it('Validar método validateInData() cuando el dato no es Array numérico', () => {
  assert.equal(operations.validateInData(2), false);
})
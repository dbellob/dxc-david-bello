const constants = require('./constants');

function suma(inData) {
    let result = inData[constants.INT_VALUE_ZERO];
    for (i = constants.INT_VALUE_ONE; i < inData.length; i++) {
        result += inData[i];
    }
    return result;
};

function resta(inData) {
    let result = inData[constants.INT_VALUE_ZERO];
    for (i = constants.INT_VALUE_ONE; i < inData.length; i++) {
        result -= inData[i];
    }
    return result;
};

function multiplicacion(inData) {
    let result = inData[constants.INT_VALUE_ZERO];
    for (i = constants.INT_VALUE_ONE; i < inData.length; i++) {
        result *= inData[i];
    }
    return result;
};

function division(inData) {
    let result = inData[constants.INT_VALUE_ZERO];
    for (i = constants.INT_VALUE_ONE; i < inData.length; i++) {
        if (inData[i] != constants.INT_VALUE_ZERO) {
            result /= inData[i];
        } else {
            return constants.UNDEFINED_DIVISION_MESSAGE;
        }
    }
    return result;
};

function validateInData(inData) {
    let validationStatus = true;
    if (Array.isArray(inData) && inData.length > constants.INT_VALUE_ZERO) {
        inData.forEach(function (element) {
            if (typeof element != constants.TYPE_OF_NUMBER) {
                validationStatus = false;
            }
        });
    } else {
        validationStatus = false;
    }
    return validationStatus;
};

module.exports = { suma, resta, multiplicacion, division, validateInData }
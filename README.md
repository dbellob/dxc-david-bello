# dxc-david-bello
#### Pre-requisitos para ejecutar la aplicación o los test unitarios
1. Tener NodeJs instalado (El computador donde se desarrolló cuenta con la versión 6.9.0)
2. Ejecutar por consola el comando $ npm install
#### Comando para ejecutar la aplicación
$ node index.js
#### Comando para ejecutar los test unitarios con reporte de coverage
$ npm test
#### Prueba del API REST
1. El API REST se debe consumir por POST desde el endpoint /test y el puerto 3000
2. El objeto que se envía en el body tiene la siguiente estructura:
{
"numeros":[1,2,3]
}
